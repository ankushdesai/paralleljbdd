// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import org.sosy_lab.pjbdd.bdd.BDDNode;
import org.sosy_lab.pjbdd.cbdd.CBDDNode;

/**
 * Entry point for environment creation. Contains builder creation methods for {@link
 * org.sosy_lab.pjbdd.api.BDDCreatorBuilder}, {@link org.sosy_lab.pjbdd.api.ZDDBuilder}, {@link
 * org.sosy_lab.pjbdd.api.IntCreatorBuilder}.
 *
 * @author Stephan Holzner
 * @see org.sosy_lab.pjbdd.api.IntCreatorBuilder
 * @see org.sosy_lab.pjbdd.api.BDDCreatorBuilder
 * @see org.sosy_lab.pjbdd.api.ZDDBuilder
 * @since 1.0
 */
public final class Builders {

  /** Avoid instantiation. */
  private Builders() {}

  /**
   * instantiate new {@link org.sosy_lab.pjbdd.api.ZDDBuilder} with default node factory.
   *
   * @return new {@link org.sosy_lab.pjbdd.api.ZDDBuilder}
   */
  public static ZDDBuilder zddBuilder() {
    return new ZDDBuilder(new BDDNode.Factory());
  }

  /**
   * instantiate new {@link org.sosy_lab.pjbdd.api.BDDCreatorBuilder} with default node factory.
   *
   * @return new {@link org.sosy_lab.pjbdd.api.BDDCreatorBuilder}
   */
  public static CreatorBuilder bddBuilder() {
    return new BDDCreatorBuilder(new BDDNode.Factory());
  }

  /**
   * instantiate new {@link org.sosy_lab.pjbdd.api.CBDDCreatorBuilder} with default node factory.
   *
   * @return new {@link org.sosy_lab.pjbdd.api.BDDCreatorBuilder}
   */
  public static CreatorBuilder cbddBuilder() {
    return new CBDDCreatorBuilder(new CBDDNode.Factory());
  }

  /**
   * instantiate new {@link org.sosy_lab.pjbdd.api.IntCreatorBuilder}.
   *
   * @return new {@link org.sosy_lab.pjbdd.api.IntCreatorBuilder}
   */
  public static CreatorBuilder intBuilder() {
    return new IntCreatorBuilder();
  }

  /** Implemented table types. */
  public enum TableType {
    ConcurrentHashBucket,
    ConcurrentHashMap,
    CASArray
  }

  /** Implemented parallelization types. */
  public enum ParallelizationType {
    FORK_JOIN,
    COMPLETABLE_FUTURE,
    FUTURE,
    GUAVA_FUTURE,
    STREAM,
    NONE
  }
}
