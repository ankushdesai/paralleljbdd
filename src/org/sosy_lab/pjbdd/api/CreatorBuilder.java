// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import org.sosy_lab.pjbdd.util.threadpool.ParallelismManager;

/**
 * Builder interface to create {@link Creator} environments.
 *
 * @author Stephan Holzner
 * @see Creator
 * @since 1.0
 */
public interface CreatorBuilder {

  /**
   * Creates a new Creator environment with previously set parameters.
   *
   * @return a new BDD environment
   */
  Creator build();

  CreatorBuilder setUseApply(boolean apply);

  CreatorBuilder setTableType(Builders.TableType type);

  CreatorBuilder setParallelismManager(ParallelismManager manager);

  CreatorBuilder setParallelism(int parallelism);

  CreatorBuilder setTableSize(int tableSize);

  CreatorBuilder setCacheSize(int cacheSize);

  CreatorBuilder setThreads(int selectedThreads);

  CreatorBuilder setVarCount(int selectedVarCount);

  CreatorBuilder setIncreaseFactor(int selectedIncreaseFactor);

  CreatorBuilder setParallelizationType(Builders.ParallelizationType type);

  CreatorBuilder disableThreadSafety();

  CreatorBuilder synchronizeReorderingOperations();
}
