// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.api;

import java.util.List;
import org.sosy_lab.pjbdd.bdd.BDDCreator;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.node.NodeManager;

public abstract class AbstractCreator {

  protected final NodeManager<DD> nodeManager;

  protected final SatAlgorithm<DD> satAlgorithm;

  /**
   * Creates new {@link BDDCreator} instances with given parameters.
   *
   * @param nodeManager - the node manager used
   * @param satAlgorithm - the sat algorithm used
   */
  protected AbstractCreator(NodeManager<DD> nodeManager, SatAlgorithm<DD> satAlgorithm) {
    this.nodeManager = nodeManager;
    this.satAlgorithm = satAlgorithm;
  }

  /** shutdown creator. */
  public void shutDown() {
    nodeManager.shutdown();
  }

  /**
   * set new variable order.
   *
   * @param pOrder - the new order
   */
  public void setVarOrder(List<Integer> pOrder) {
    nodeManager.setVarOrder(pOrder);
  }

  /**
   * get current variable count.
   *
   * @return current variable count
   */
  public int getVariableCount() {
    return nodeManager.getVarCount();
  }

  /**
   * get current variable order.
   *
   * @return current variable order.
   */
  public int[] getVariableOrdering() {
    return nodeManager.getCurrentOrdering();
  }

  /**
   * set new variable count.
   *
   * @param count - the new count.
   */
  public void setVariableCount(int count) {
    nodeManager.setVarCount(count);
  }
}
