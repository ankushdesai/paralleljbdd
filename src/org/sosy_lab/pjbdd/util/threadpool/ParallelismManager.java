// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.util.threadpool;

import java.util.concurrent.ForkJoinPool;

/**
 * Interface for worker thread pool manager.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public interface ParallelismManager {

  /**
   * get the actual worker thread pool.
   *
   * @return the actual worker thread pool
   */
  ForkJoinPool getThreadPool();

  /**
   * get worker thread pool's number of threads.
   *
   * @return worker thread pool's number of threads
   */
  int getParallelism();

  /**
   * Determine if submitting a new task would be reasonable, to avoid over threading.
   *
   * @param topLevel - the topmost bdd level
   * @return <code>true</code> if forking would be reasonable, <code>false</code> else
   */
  boolean canFork(int topLevel);

  /** new task submitted. */
  void taskSupplied();

  /** task done. */
  void taskDone();

  /** safely shutdown thread pool. */
  void shutdown();
}
