// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd.algorithm;

import java.util.Optional;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.bdd.AbstractBDDAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/** Interface for algorithm implementations to perform BDD manipulation. */
public class ITEBDDAlgorithm<V extends DD> extends AbstractBDDAlgorithm<V> {

  public ITEBDDAlgorithm(
      Cache<Integer, Cache.CacheData> computedTable, NodeManager<V> nodeManager) {
    super(computedTable, nodeManager);
  }

  /** {@inheritDoc} */
  @Override
  public V makeOp(V f1, V f2, ApplyOp op) {

    switch (op) {
      case OP_OR:
        return makeIte(f1, makeTrue(), f2);
      case OP_AND:
        return makeIte(f1, f2, makeFalse());
      case OP_XOR:
        return makeIte(f1, makeNot(f2), f2);
      case OP_NOR:
        return makeIte(f1, makeFalse(), makeNot(f2));
      case OP_NAND:
        return makeIte(f1, makeNot(f2), makeTrue());
      case OP_IMP:
        return makeOp(makeNot(f1), f2, ApplyOp.OP_OR);
      case OP_XNOR:
        return makeIte(f1, f2, makeNot(f2));
    }
    return makeFalse();
  }

  /** {@inheritDoc} */
  @Override
  public V makeNot(V f) {
    return makeIte(f, makeFalse(), makeTrue());
  }

  @Override
  public V makeIte(V ifBDD, V thenBDD, V elseBDD) {
    return terminalIteCheck(ifBDD, thenBDD, elseBDD)
        .orElseGet(
            () -> {
              int topVar = topVar(level(ifBDD), level(thenBDD), level(elseBDD));
              V low = makeIte(low(ifBDD, topVar), low(thenBDD, topVar), low(elseBDD, topVar));
              V high = makeIte(high(ifBDD, topVar), high(thenBDD, topVar), high(elseBDD, topVar));
              V res = makeNode(low, high, topVar);
              cacheItem(ifBDD, thenBDD, elseBDD, res);
              return res;
            });
  }

  /*
    * ITE-optimization detect redundant triples.
    *
    * @param f1 - if branch
    * @param f2 - then branch
    * @param f3 - else branch
    * @return Optional of redundancy hit or empty

   private Optional<BDD> checkComplementTriple(BDD f1, BDD f2, BDD f3) {
     if (f1.equals(f2)) {
       return Optional.of(shannonExpansionIte(f1, makeTrue(), f3));
     }
     if (f1.equals(f3)) {
       return Optional.of(shannonExpansionIte(f1, f2, makeFalse()));
     }
     if (f2.isTrue() && level(f3) > level(f1)) {
       return Optional.of(shannonExpansionIte(f3, f2, f1));
     }
     if (f3.isFalse() && level(f2) > level(f1)) {
       return Optional.of(shannonExpansionIte(f2, f1, f3));
     }
     return Optional.empty();
   }
  */

  /**
   * Chain base-case check and cache check.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of (base-case/cache) hit or empty
   */
  protected Optional<V> terminalIteCheck(V f1, V f2, V f3) {
    return checkTerminalCases(f1, f2, f3).or(() -> checkITECache(f1, f2, f3));
  }

  /**
   * check if 'ITE' input triple matches terminal case.
   *
   * @param f1 - if branch
   * @param f2 - then branch
   * @param f3 - else branch
   * @return Optional of matched case or Optional empty
   */
  protected Optional<V> checkTerminalCases(V f1, V f2, V f3) {
    if (f1.equals(makeTrue())) {
      return Optional.of(f2);
    }
    if (f1.equals(makeFalse())) {
      return Optional.of(f3);
    }
    if (f2.equals(f3)) {
      return Optional.of(f2);
    }
    if (f2.equals(makeTrue()) && f3.equals(makeFalse())) {
      return Optional.of(f1);
    }
    return Optional.empty();
  }
}
