// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.bdd;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.core.algorithm.SatAlgorithm;
import org.sosy_lab.pjbdd.core.cache.Cache;
import org.sosy_lab.pjbdd.core.node.NodeManager;

/**
 * BDD based {@link SatAlgorithm} implementation. Uses serial algorithms for bdd sat operations.
 *
 * @author Stephan Holzner
 * @see SatAlgorithm
 * @since 1.1
 */
public class BDDSat<V extends DD> implements SatAlgorithm<V> {

  /** {@link Cache} used for computed sat counts. */
  protected final Cache<V, BigInteger> satCountCache;

  protected final NodeManager<V> nodeManager;

  public BDDSat(Cache<V, BigInteger> satCountCache, NodeManager<V> nodeManager) {
    this.satCountCache = satCountCache;
    this.nodeManager = nodeManager;
  }

  /** {@inheritDoc} */
  @Override
  public V anySat(V bdd) {
    if (bdd.isLeaf()) {
      return bdd;
    }
    if (bdd.getLow().isFalse()) {
      return nodeManager.makeNode(nodeManager.getFalse(), anySat(high(bdd)), bdd.getVariable());
    } else {
      return nodeManager.makeNode(anySat(low(bdd)), nodeManager.getFalse(), bdd.getVariable());
    }
  }

  /** {@inheritDoc} */
  @Override
  public BigInteger satCount(V b) {
    return BigInteger.valueOf(2).pow(level(b)).multiply(satCountRec(b));
  }

  /**
   * recursive calculation of number of possible satisfying truth assignments for given bdd.
   *
   * @param root - a bdd
   * @return root's number of possible satisfying truth assignments
   */
  protected BigInteger satCountRec(V root) {
    if (root.isFalse()) {
      return BigInteger.ZERO;
    }
    if (root.isTrue()) {
      return BigInteger.ONE;
    }
    BigInteger count = satCountCache.get(root);
    if (count != null) {
      return count;
    }

    BigInteger s = BigInteger.valueOf(2).pow((level(low(root)) - level(root) - 1));
    BigInteger size = s.multiply(satCountRec(low(root)));

    s = BigInteger.valueOf(2).pow((level(high(root)) - level(root) - 1));
    size = size.add(s.multiply(satCountRec(high(root))));

    satCountCache.put(root, size);

    return size;
  }

  /**
   * helper call to determine bdd's variable level.
   *
   * @param bdd - the bdd
   * @return level of bdd
   */
  protected int level(V bdd) {
    return nodeManager.level(bdd.getVariable());
  }

  /**
   * helper call to determine bdd's high successor.
   *
   * @param bdd - the bdd
   * @return high successor
   */
  protected V high(V bdd) {
    return nodeManager.getHigh(bdd);
  }

  /**
   * helper call to determine bdd's low successor.
   *
   * @param bdd - the bdd
   * @return low successor
   */
  protected V low(V bdd) {
    return nodeManager.getLow(bdd);
  }
}
