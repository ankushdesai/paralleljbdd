// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.intBDD;

import java.lang.invoke.MethodHandles;
import java.lang.invoke.VarHandle;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Main IntUniqueTable implementation. Concurrent IntUniqueTable implementation which underlying
 * uses a resizing int array as data structure and a compare and swap strategy with {@link
 * VarHandle} to guarantee thread-safe and concurrent access.
 *
 * @author Stephan Holzner
 * @see IntUniqueTable
 * @since 1.0
 */
public class CasIntUniqueTable extends IntUniqueTableImpl implements IntUniqueTable {

  /** Use VarHandle for atomic int swap operations. */
  private static final VarHandle arrayHandle = MethodHandles.arrayElementVarHandle(int[].class);

  private static final VarHandle FREE_COUNTER_HANDLE;
  private static final VarHandle NEXT_FREE_HANDLE;

  static {
    try {
      FREE_COUNTER_HANDLE =
          MethodHandles.lookup().findVarHandle(IntUniqueTableImpl.class, "freeCounter", int.class);
      NEXT_FREE_HANDLE =
          MethodHandles.lookup().findVarHandle(IntUniqueTableImpl.class, "nextFree", int.class);
    } catch (ReflectiveOperationException e) {
      throw new AssertionError(e);
    }
  }

  /** Read access counter for resize synchronization. */
  private final AtomicInteger readAccesses = new AtomicInteger();

  /**
   * Creates a new {@link CasIntUniqueTable} with specified initial size, parallelism and increase
   * factor.
   *
   * @param initialSize - the initial table size
   * @param increaseFactor - the table's increase factor
   * @param disableGC - flag to disable reference counting and garbage collection
   */
  public CasIntUniqueTable(int initialSize, int increaseFactor, boolean disableGC) {
    super(initialSize, increaseFactor, disableGC);
  }

  /** {@inheritDoc} */
  @Override
  public int getOrCreate(int var, int low, int high) {
    // Add reference for Nodes in use to avoid gbc while resize
    boolean resize = false;
    boolean computationComplete = false;
    int res;
    tryResizing();
    try {
      readAccesses.incrementAndGet();
      int hash = hashNode(var, low, high);
      res = getNodeWithHash(hash);
      int head = res;
      while (res != 0) {
        if (getVariable(res) == var && getLow(res) == low && getHigh(res) == high) {
          computationComplete = true;
          break;
        }
        res = getNext(res);
      }
      if (!computationComplete) {
        res = getNextFree();
        if (res < 2) {
          resize = true;
        } else {
          computationComplete = createNode(res, low, high, var, hash, head);
        }
      }

    } finally {
      readAccesses.decrementAndGet();
    }

    if (resize) {
      tryResizing();
    }
    if (!computationComplete) {
      return getOrCreate(var, low, high);
    }
    return res;
  }

  @Override
  protected boolean createNode(int node, int low, int high, int var, int hash, int head) {
    if (!setHashAtomic(hash, node, head)) {
      // maybe set node as next free? or just wait for next garbage collection
      return false;
    }
    setNext(node, head);
    setVariable(node, var);
    setLow(node, low);
    setHigh(node, high);
    atomicDecrementFreeCounter();
    return true;
  }

  private final Lock resizeMonitor = new ReentrantLock();
  private final Object readMonitor = new Object();

  private boolean resizeInProgress = false;

  /** Try to resize table. Wait if resize op is already in progress. Collect all locks else. */
  private void tryResizing() {
    if (checkResize()) {
      synchronized (readMonitor) {
        readMonitor.notify();
      }
      resizeMonitor.lock();

      try {
        if (checkResize()) { // resize performed
          resizeInProgress = true;

          while (readAccesses.get() > 0) {
            try {
              synchronized (readMonitor) {
                readMonitor.wait(1000);
              }
            } catch (InterruptedException ignored) {
            }
          }
          try {
            resizeTable();
          } finally {
            resizeInProgress = false;
          }
        }
      } finally {
        resizeMonitor.unlock();
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public void setMaxRef(int node) {
    if (!disableGC) {
      arrayHandle.set(table, node * nodeSize + OFFSET_REF_COUNT, MAX_REF);
    }
  }

  /** {@inheritDoc} */
  @Override
  public void incRef(int... roots) {
    if (disableGC) {
      return;
    }
    for (int r : roots) {
      if (table[r * nodeSize + OFFSET_REF_COUNT] != MAX_REF) {
        arrayHandle.getAndAdd(table, r * nodeSize + OFFSET_REF_COUNT, 1);
      }
    }
  }

  /** {@inheritDoc} */
  @Override
  public int getNodeCount() {
    return nodeCount - atomicReadFreeCounter();
  }

  /** {@inheritDoc} */
  @Override
  public void decRef(int... roots) {
    if (disableGC) {
      return;
    }
    for (int r : roots) {
      int old = table[r * nodeSize + OFFSET_REF_COUNT];
      if (old != MAX_REF && old > 0) {
        arrayHandle.getAndAdd(table, r * nodeSize + OFFSET_REF_COUNT, -1);
      }
    }
  }

  @Override
  protected boolean hasRef(int r) {
    return !arrayHandle.get(table, r * nodeSize + OFFSET_REF_COUNT).equals(0);
  }

  /**
   * Set hash for given index.
   *
   * @param hash bdd's hash value
   * @param index bdd's index
   */
  protected boolean setHashAtomic(int hash, int index, int old) {
    // this function needs to be atomic for same hash
    if (old == INVALID_BDD) {
      table[hash * nodeSize + OFFSET_HASH] = index;
      return true;
    } else {
      return arrayHandle.weakCompareAndSetPlain(table, hash * nodeSize + OFFSET_HASH, old, index);
    }
  }

  private void atomicDecrementFreeCounter() {
    FREE_COUNTER_HANDLE.getAndAdd(this, -1);
  }

  private int atomicReadFreeCounter() {
    return (int) FREE_COUNTER_HANDLE.get(this);
  }

  private boolean atomicSetNextFree(int value, int old) {
    return NEXT_FREE_HANDLE.weakCompareAndSetPlain(this, old, value);
  }

  @Override
  protected int getNextFree() {
    int next = (int) NEXT_FREE_HANDLE.get(this);
    if (atomicSetNextFree(getNext(next), next)) {
      return next;
    }
    return getNextFree();
  }

  /**
   * Checks if table's reaches threshold and table resize must be performed.
   *
   * @return true if minimal threshold of free nodes reached or {@link #resizeInProgress} else
   */
  @Override
  protected boolean checkResize() {
    if (nextFree > 1) {
      return resizeInProgress;
    }
    return true;
  }
}
