package org.sosy_lab.pjbdd.intBDD.cache;

/**
 * Data implementation.
 *
 * @author Stephan Holzner
 * @since 1.0
 */
public class OpCacheData {
  /** cached input (a,b,c) and result(res) values. */
  private final int a;

  private final int b;
  private final int c;
  private final int res;

  /**
   * Creates new IntITEData with given input and result values.
   *
   * @param a - getIf input value
   * @param b - getThen input value
   * @param c - getElse input value
   * @param res - result value
   */
  OpCacheData(int a, int b, int c, int res) {
    this.a = a;
    this.b = b;
    this.c = c;
    this.res = res;
  }

  public int result() {
    return res;
  }

  public int f1() {
    return a;
  }

  public int f2() {
    return b;
  }

  public int f3() {
    return c;
  }
}
