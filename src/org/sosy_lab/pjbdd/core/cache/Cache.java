// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.cache;

import org.sosy_lab.pjbdd.api.DD;
import org.sosy_lab.pjbdd.util.IntArrayUtils;

/**
 * Interfaces for computation cache classes.
 *
 * @param <K> cache key type
 * @param <V> cache value type
 * @author Stephan Holzner
 * @since 1.0
 */
public interface Cache<K, V> {

  /**
   * init cache with given parameters.
   *
   * @param cacheSize - the fixed cache size
   * @param parallelism - number of concurrent accesses
   */
  void init(int cacheSize, int parallelism);

  /** clear cache. */
  void clear();

  /**
   * save value for given key.
   *
   * @param key - given key
   * @param value - to save
   */
  void put(K key, V value);

  /**
   * get value for given key.
   *
   * @param key - given key
   * @return value or null if no such key
   */
  V get(K key);

  /**
   * Clone the existing table without values.
   *
   * @return a copy
   */
  Cache<K, V> cleanCopy();

  int nodeCount();

  int size();

  /**
   * {@link CacheData} implementation for computation caching with two input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataBinaryOp<V extends DD> implements CacheData {
    private V f1;
    private V f2;
    private V res;

    public V getF1() {
      return f1;
    }

    public void setF1(V f1) {
      this.f1 = f1;
    }

    public V getF2() {
      return f2;
    }

    public void setF2(V f2) {
      this.f2 = f2;
    }

    public V getRes() {
      return res;
    }

    public void setRes(V res) {
      this.res = res;
    }

    public int getOp() {
      return op;
    }

    public void setOp(int op) {
      this.op = op;
    }

    private int op;

    public boolean matches(V tF1, V tF2, int var) {
      return tF1.equals(getF1()) && tF2.equals(getF2()) && var == getOp();
    }
  }

  /**
   * {@link CacheData} implementation for computation caching exquant input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataExQuantVararg<V extends DD> implements CacheData {
    private V f;
    private V res;
    private int[] levels;

    public boolean matches(V node, int... mValues) {
      return f.equals(node) && IntArrayUtils.equals(levels, mValues);
    }

    public V getF() {
      return f;
    }

    public void setF(V f) {
      this.f = f;
    }

    public V getRes() {
      return res;
    }

    public void setRes(V res) {
      this.res = res;
    }

    public void setLevels(int... levels) {
      this.levels = levels;
    }
  }

  /**
   * {@link CacheData} implementation for computation caching negated input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataExQuant<V extends DD> implements CacheData {
    private V f;
    private V res;
    private int level;

    public V getF() {
      return f;
    }

    public void setF(V f) {
      this.f = f;
    }

    public V getRes() {
      return res;
    }

    public void setRes(V res) {
      this.res = res;
    }

    public int getLevel() {
      return level;
    }

    public void setLevel(int level) {
      this.level = level;
    }
  }

  /**
   * {@link CacheData} implementation for computation caching negated input values.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataNot<V extends DD> implements CacheData {
    private V f;
    private V res;

    public V getF() {
      return f;
    }

    public void setF(V f) {
      this.f = f;
    }

    public V getRes() {
      return res;
    }

    public void setRes(V res) {
      this.res = res;
    }
  }

  /**
   * Empty interface as most general data type to be cached in computation caches.
   *
   * @author Stephan Holzner
   * @since 1.0
   */
  interface CacheData {}

  /**
   * {@link CacheData} implementation for 'ITE' computation caching.
   *
   * @author Stephan Holzner
   * @see CacheData
   * @since 1.0
   */
  class CacheDataITE<V extends DD> implements CacheData {
    private V f1;
    private V f2;
    private V f3;

    public V getF1() {
      return f1;
    }

    public void setF1(V f1) {
      this.f1 = f1;
    }

    public V getF2() {
      return f2;
    }

    public void setF2(V f2) {
      this.f2 = f2;
    }

    public V getF3() {
      return f3;
    }

    public void setF3(V f3) {
      this.f3 = f3;
    }

    public V getRes() {
      return res;
    }

    public void setRes(V res) {
      this.res = res;
    }

    private V res;
  }
}
