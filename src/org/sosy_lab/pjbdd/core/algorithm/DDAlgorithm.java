// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.core.algorithm;

import org.sosy_lab.pjbdd.api.DD;

/** Interface for algorithm implementations to perform DD manipulation . */
public interface DDAlgorithm<V extends DD> extends Algorithm<V> {

  /**
   * Performs binary BDD manipulation operations.
   *
   * @param f1 - the first bdd node
   * @param f2 - the second bdd node
   * @param op - the operation to be performed
   * @return f1 'op' f2
   */
  V makeOp(V f1, V f2, ApplyOp op);

  /**
   * Performs if then else computation on input triple.
   *
   * @param f1 - the if bdd node
   * @param f2 - the then bdd node
   * @param f3 - the else bdd node
   * @return ite (f1, f2, f3)
   */
  V makeIte(V f1, V f2, V f3);

  /**
   * Negates bdd argument.
   *
   * @param b - the bdd argument
   * @return not b
   */
  V makeNot(V b);

  /**
   * Creates a {@link DD} representing an existential quantification of the getThen argument.
   *
   * @param f1 - getIf {@link DD} argument
   * @param levels - the variable levels to be quantified
   * @return (exists f2 : f1)
   */
  V makeExists(V f1, int... levels);

  V makeCompose(V f1, int var, V f2);

  /**
   * Enum with 'Apply' operation types. Each type belongs to one binary method {@link ApplyOp}s
   * corresponding to following methods.
   *
   * <ul>
   *   <li>{@link ApplyOp#OP_AND}: Perform boolean AND for to arguments
   *   <li>{@link ApplyOp#OP_XOR}: Perform boolean XOR for to arguments
   *   <li>{@link ApplyOp#OP_OR}: Perform boolean OR for to arguments
   *   <li>{@link ApplyOp#OP_NAND}: Perform boolean NAND for to arguments
   *   <li>{@link ApplyOp#OP_NOR}: Perform boolean NOR for to arguments
   *   <li>{@link ApplyOp#OP_IMP}: Perform boolean IMP for to arguments
   *   <li>{@link ApplyOp#OP_XNOR}: Perform boolean OP_XNOR for to arguments
   * </ul>
   *
   * @author Stephan Holzner
   * @version 1.0
   */
  enum ApplyOp {
    OP_AND,
    OP_XOR,
    OP_OR,
    OP_NAND,
    OP_NOR,
    OP_IMP,
    OP_XNOR
  }
}
