// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'COMPOSE' test class uses {@link CreatorCombinatorTest} as base class to perform make
 * 'COMPOSE' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class ComposeTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  @Test
  public void test() {
    skipIfChained();

    DD a = creator.makeIthVar(1);
    DD b = creator.makeIthVar(2);
    DD c = creator.makeIthVar(3);
    DD d = creator.makeIthVar(4);
    DD e = creator.makeIthVar(5);

    DD bdd1 =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), c))));
    DD bdd2 = creator.makeAnd(d, e);

    DD composition = creator.makeCompose(bdd1, c.getVariable(), bdd2);

    DD expected =
        creator.makeOr(
            creator.makeAnd(creator.makeNot(a), b),
            creator.makeAnd(a, creator.makeOr(b, creator.makeAnd(creator.makeNot(b), bdd2))));

    assertEquals(expected, composition);

    creator.shutDown();
  }
}
