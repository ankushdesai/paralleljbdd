// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.api.DD;

public class DivTest extends ZDDCombinatorTest {
  @Override
  public void test() {
    DD var0 = creator.makeNode(creator.empty(), creator.base(), 0);
    DD var1 = creator.makeNode(creator.empty(), creator.base(), 1);
    DD var2 = creator.makeNode(creator.empty(), creator.base(), 2);

    DD bdd0 = creator.union(var2, var1);
    DD bdd1 = creator.union(bdd0, var0);

    DD div0 = creator.division(creator.empty(), bdd1);
    assertEquals(creator.empty(), div0);

    DD div1 = creator.division(creator.base(), bdd1);
    assertEquals(creator.empty(), div1);

    DD div2 = creator.division(bdd1, bdd1);
    assertEquals(creator.base(), div2);

    DD div3 = creator.division(bdd1, creator.base());
    assertEquals(bdd1, div3);
  }
}
