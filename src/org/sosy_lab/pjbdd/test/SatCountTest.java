// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import java.math.BigInteger;
import org.sosy_lab.pjbdd.examples.NQueens;

/**
 * Make 'SAT_COUNT' test class uses {@link CreatorCombinatorTest} as base class to perform make
 * 'SAT_COUNT' test for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class SatCountTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {

    NQueens queens = new NQueens(7, creator);
    queens.build();
    assertEquals(BigInteger.valueOf(40), queens.solve());

    NQueens queens2 = new NQueens(8, creator);
    queens2.build();
    assertEquals(BigInteger.valueOf(92), queens2.solve());

    creator.shutDown();
  }
}
