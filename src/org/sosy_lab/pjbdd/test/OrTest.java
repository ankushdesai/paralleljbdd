// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'OR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'OR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class OrTest extends CreatorCombinatorTest {
  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeOr(var1, var1);
    assertEquals(res1, var1);

    DD res2 = creator.makeOr(var1, creator.makeFalse());
    assertEquals(res2, var1);

    DD res3 = creator.makeOr(var1, creator.makeTrue());
    Assert.assertEquals(res3, creator.makeTrue());

    DD res4 = creator.makeOr(creator.makeTrue(), var2);
    Assert.assertEquals(res4, creator.makeTrue());

    DD res5 = creator.makeOr(creator.makeFalse(), var2);
    assertEquals(res5, var2);
    creator.shutDown();
  }
}
