// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import org.junit.Assert;
import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'NOR' test class uses {@link CreatorCombinatorTest} as base class to perform make 'NOR' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class NorTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {
    DD var1 = creator.makeVariable();
    DD var2 = creator.makeVariable();

    DD res1 = creator.makeNor(var1, creator.makeTrue());
    Assert.assertEquals(res1, creator.makeFalse());

    DD res2 = creator.makeNor(creator.makeTrue(), var2);
    Assert.assertEquals(res2, creator.makeFalse());
    creator.shutDown();
  }
}
