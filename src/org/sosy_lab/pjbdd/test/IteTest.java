// This file is part of PJBDD,
// a framework for decision diagrams:
// https://gitlab.com/sosy-lab/software/paralleljbdd
//
// SPDX-FileCopyrightText: 2020 Dirk Beyer <https://www.sosy-lab.org>
//
// SPDX-License-Identifier: Apache-2.0

package org.sosy_lab.pjbdd.test;

import static org.junit.Assert.assertEquals;

import org.sosy_lab.pjbdd.api.DD;

/**
 * Make 'ITE' test class uses {@link CreatorCombinatorTest} as base class to perform make 'ITE' test
 * for all known creators.
 *
 * @author Stephan Holzner
 * @see CreatorCombinatorTest
 * @since 1.0
 */
public class IteTest extends CreatorCombinatorTest {

  /** {@inheritDoc} */
  @Override
  public void test() {

    DD i = creator.makeVariable();
    DD t = creator.makeVariable();
    DD e = creator.makeVariable();

    DD res1 = creator.makeIte(i, t, t);
    assertEquals(res1, t);

    DD res2 = creator.makeIte(creator.makeTrue(), t, e);
    assertEquals(res2, t);

    DD res3 = creator.makeIte(creator.makeFalse(), t, e);
    assertEquals(res3, e);

    DD res4 = creator.makeIte(i, creator.makeTrue(), creator.makeFalse());
    assertEquals(res4, i);

    DD notI = creator.makeNot(i);
    DD res5 = creator.makeIte(i, creator.makeFalse(), creator.makeTrue());
    assertEquals(res5, notI);
    creator.shutDown();
  }
}
